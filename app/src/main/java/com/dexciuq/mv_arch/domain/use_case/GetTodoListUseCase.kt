package com.dexciuq.mv_arch.domain.use_case

import com.dexciuq.mv_arch.domain.model.Todo
import com.dexciuq.mv_arch.domain.repository.TodoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetTodoListUseCase(
    private val repository: TodoRepository
) {
    operator fun invoke(): List<Todo> {
        return repository.getTodoList()
    }
}