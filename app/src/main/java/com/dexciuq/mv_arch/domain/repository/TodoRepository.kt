package com.dexciuq.mv_arch.domain.repository

import com.dexciuq.mv_arch.domain.model.Todo

interface TodoRepository {
    fun getTodoList(): List<Todo>
}