package com.dexciuq.mv_arch.data.repository

import com.dexciuq.mv_arch.data.source.DataSource
import com.dexciuq.mv_arch.domain.model.Todo
import com.dexciuq.mv_arch.domain.repository.TodoRepository

class TodoRepositoryImpl(
    private val dataSource: DataSource
) : TodoRepository {
    override fun getTodoList(): List<Todo> = dataSource.getTodoList()
}