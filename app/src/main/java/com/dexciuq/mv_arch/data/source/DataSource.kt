package com.dexciuq.mv_arch.data.source

import com.dexciuq.mv_arch.domain.model.Todo

interface DataSource {
    fun getTodoList(): List<Todo>
}