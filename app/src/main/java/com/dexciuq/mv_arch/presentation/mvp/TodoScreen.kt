package com.dexciuq.mv_arch.presentation.mvp

import com.dexciuq.mv_arch.domain.model.Todo

sealed interface TodoScreen {
    interface View {
        fun showMessage(todo: Todo)
        fun showTodoList(todoList: List<Todo>)
    }

    interface Presenter {
        fun getTodoList()
        fun onTodoClick(todo: Todo)
    }
}