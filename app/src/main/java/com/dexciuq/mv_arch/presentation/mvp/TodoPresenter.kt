package com.dexciuq.mv_arch.presentation.mvp

import com.dexciuq.mv_arch.data.repository.TodoRepositoryImpl
import com.dexciuq.mv_arch.data.source.LocalDataSource
import com.dexciuq.mv_arch.domain.model.Todo
import com.dexciuq.mv_arch.domain.use_case.GetTodoListUseCase

class TodoPresenter(
    private val view: TodoScreen.View
) : TodoScreen.Presenter {

    private val getTodoListUseCase: GetTodoListUseCase = GetTodoListUseCase(TodoRepositoryImpl(LocalDataSource))

    override fun getTodoList() {
        view.showTodoList(getTodoListUseCase())
    }

    override fun onTodoClick(todo: Todo) {
        view.showMessage(todo)
    }
}